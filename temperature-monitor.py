#!/usr/bin/python
import time
import math
import plotly
from time import gmtime, strftime
import sys
sys.path.insert(0, './python-i2c-libraries')
from TMP175 import TMP175

# I2C device addresses
TEMP_SENSOR_ADDR = 0x48

# Class instances
Temp = TMP175(TEMP_SENSOR_ADDR)

# Main program
def main():
	''' Read current temperature and update plotly. '''
	
	# Create an instance of the plotly plotter
	py = plotly.plotly(username_or_email="jaimeshm", key="dfdmavtw97")
	
	# Read the current variables for insertion
	current_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
	current_temperature = Temp.read_temp()
	
	# Debug variable output
	print('current_time = ' + str(current_time))
	print('current_temperature = ' + str(current_temperature))
	# time.sleep(5)
	
	# Format the line graph
	x_axesstyle = {
		"title" : "Time",
	}
	
	y_axesstyle = {
		"title" : "Temperature (Celsius)",
	}

	layout = {
		"title" : "Raspberry Pi Temperature Logger",
		"xaxis" : x_axesstyle,
		"yaxis" : y_axesstyle,
	}
	
	# Send API call
	response = py.plot(current_time, current_temperature, filename= 'RPi Temperature Logger/RPi Temperature Log', fileopt='extend', layout=layout)
	
if __name__ == "__main__":
	main()
	