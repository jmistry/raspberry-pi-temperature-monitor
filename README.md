Raspberry Pi Temperature Logger using Texas Instruments TMP175
====================

This is a python program for the Raspberry Pi which makes use of a Texas Instruments TMP175 temperature sensor to log data to [plotly](http://plot.ly/). See  [https://plot.ly/api/python](https://plot.ly/api/python) for plotly installation instructions. Note that this uses repository has included another repository as a git submodule - [python-i2c-libraries](https://bitbucket.org/jmistry/python-i2c-libraries).

Supported ICs:

IC Name       | Description
------------- | -------------
TMP175        | Texas Instruments digital temperature sensor with I2C interface